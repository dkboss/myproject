﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tesseract;

namespace pagingNSortingInMVC4.Controllers
{
    public class ReadIMageController : Controller
    {
        //
        // GET: /ReadIMage/

        public ActionResult Index()
        {
             var testImagePath = "";
            var dataPath = @"C:\Users\dharmendra\Downloads\tesseract-master\tesseract-master";

            try
            {  

               
                using (var tEngine = new TesseractEngine(dataPath, "eng", EngineMode.Default)) //creating the tesseract OCR engine with English as the language
                {
                    using (var img = Pix.LoadFromFile(@"F:\Testing project\pagingNSortingInMVC4\pagingNSortingInMVC4\pagingNSortingInMVC4\Images\TestImage.png")) // Load of the image file from the Pix object which is a wrapper for Leptonica PIX structure
                    {                        
                        using (var page = tEngine.Process(img)) //process the specified image
                        {
                            var text = page.GetText(); //Gets the image's content as plain text.
                            Console.WriteLine(text); //display the text
                            Console.WriteLine(page.GetMeanConfidence()); //Get's the mean confidence that as a percentage of the recognized text.
                            Console.ReadKey();                            
                        }
                    }
                }
            }
            catch (Exception e)
            {                
                Console.WriteLine("Unexpected Error: " + e.Message);
            }
            return View();
        }

    }
}
