﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcExtension
{

    public static class MyHeleprs
    {
        //public static MvcHtmlString Image(this HtmlHelper htmlHelper,
        //    string source, string alternativeText)
        //{
        //    //declare the html helper 
        //    var builder = new TagBuilder("image");
        //    //hook the properties and add any required logic

        //    builder.MergeAttribute("src", source);
        //    builder.MergeAttribute("alt", alternativeText);
        //    //create the helper with a self closing capability
        //    return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        //}

        public static MvcHtmlString MvcGrid(this HtmlHelper htmlHelper, IEnumerable<dynamic> lstgrid, int pagesize)
        {

            model.grdlist = lstgrid.ToPagedList(1, 10);
            model.grdlist1 = lstgrid;
            
            model.htmlhelper = htmlHelper;
            StringBuilder sb = CreateGridHtml(model.grdlist, 1);

            return MvcHtmlString.Create(sb.ToString());
        }


        public static StringBuilder CreateGridHtml(PagedList.IPagedList<dynamic> lstgrid, int pagesize)
        {
            HtmlHelper htmlHelper = model.htmlhelper;
            StringBuilder sb = new StringBuilder();

            sb.Append("<table>");
            sb.Append("<tr>");

            List<dynamic> list = lstgrid.Cast<dynamic>().ToList();
            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();

            if (list != null && list.Count > 0)
            {
                props = TypeDescriptor.GetProperties(list[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                if (props != null)
                {
                    object[] values = new object[props.Count];
                    foreach (dynamic item in lstgrid)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = props[i].GetValue(item) ?? DBNull.Value;
                        }
                        table.Rows.Add(values);
                    }
                }


            }
            //var arrayNames = (from DataColumn x in table.Columns
            //                  select x.ColumnName).ToList();         


            foreach (DataColumn dc in table.Columns)
            {
                sb.Append("<th style='border: 2px solid black; text-align: center; width: 12%>'");
                sb.AppendLine("<a href='/?sortOrder=" + dc.ColumnName + "'>" + dc.ColumnName + "</a>");
                sb.Append("</th>");
            }

            sb.Append("</tr>");


            foreach (DataRow dr in table.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn dc in table.Columns)
                {
                    sb.Append(string.Format("<td style='border: 2px solid black; text-align: center; word-wrap: break-word;'>" + dr[dc] + "</td>"));
                }
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("<br/>");
            sb.Append("<div id='Paging' style='text-align: center'>");

            sb.Append("Page" + (model.grdlist.PageCount < model.grdlist.PageNumber ? 0 : model.grdlist.PageNumber));
            sb.Append("of" + model.grdlist.PageCount);
            var url = new UrlHelper(System.Web.HttpContext.Current.Request.RequestContext);
            //sb.Append(PagedList.Mvc.HtmlHelper.PagedListPager(htmlHelper, lstgrid, page => url.Action("Paging", "Helper", new { page })));
            sb.Append(PagedList.Mvc.HtmlHelper.PagedListPager(htmlHelper, model.grdlist, page => url.Action("Paging", "Helper", new { page })));

            sb.Append("</div>");
            return sb;

            
        }



    }

}
