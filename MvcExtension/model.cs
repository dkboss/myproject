﻿using  System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcExtension
{
   public static  class model
    {
       public static  PagedList.IPagedList<dynamic> grdlist { get; set; }
       public static IEnumerable<dynamic> grdlist1 { get; set; }
       public static HtmlHelper htmlhelper { get; set; }
       public static string RedirectToroute { get;set; }
    }
}
