﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcExtension
{
    public class HelperController : Controller
    {

        public MvcHtmlString Paging(int? page)
        {
            PagedList.IPagedList<dynamic> lstgrid = null;
            StringBuilder sb = new StringBuilder();
            try
            {
              
              
                if (model.grdlist.Count() > 0)
                {
                    
                    int pageSize =2;
                    int pageIndex = 1;
                    pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;                   
                   lstgrid = model.grdlist1.ToPagedList(pageIndex, pageSize);
                    sb = MyHeleprs.CreateGridHtml(lstgrid, pageSize);
                  
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

            //return View(model.RedirectToroute, emp);

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}
